FROM php:7.4-apache-buster

# Install Git
RUN apt-get update \
    && apt-get install -y git \
    && apt-get install -y wget \
    && apt-get install -y unzip \
    && apt-get install -y nano \
    && apt-get install -y neovim \
    && apt-get install -y fish \
    && apt-get install -y mysql-client \

# install the PHP extensions we need
RUN set -eux; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
    zip \
    curl \
    unzip \
		libfreetype6-dev \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
		libzip-dev \
	; \
	\
	docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg=/usr \
	; \
	\
	docker-php-ext-install -j "$(nproc)" \
		gd \
    bcmath \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
	\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual; \
	\
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN { \
		echo 'memory_limit=512M'; \
    echo 'upload_max_filesize=100M'; \
    echo 'post_max_size=100M'; \
    echo 'max_execution_time=666'; \
	} > /usr/local/etc/php/conf.d/custom-opigno.ini


# Install pdf.js to show pdf slides and Tincan PHP
RUN mkdir pdf.js && cd pdf.js \
	&& wget https://github.com/mozilla/pdf.js/releases/download/v2.12.313/pdfjs-2.12.313-dist.zip \
	&& unzip ./pdfjs-2.12.313-dist.zip && rm ./pdfjs-2.12.313-dist.zip \
	&& cd .. && wget https://github.com/RusticiSoftware/TinCanPHP/archive/1.1.1.zip \
	&& unzip ./1.1.1.zip && rm ./1.1.1.zip && mv ./TinCanPHP-1.1.1 ./TinCanPHP

COPY --from=composer:2 /usr/bin/composer /usr/local/bin/

ENV OPIGNOL_VERSION 3.0

WORKDIR /app
COPY ./drupal .

ENV APACHE_DOCUMENT_ROOT /app/web
RUN set -eux; \
	sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf; \
	export COMPOSER_HOME="$(mktemp -d)"; \
	composer install; \
	chown -R www-data:www-data web/sites web/modules web/themes; \
	# delete composer cache
	rm -rf "$COMPOSER_HOME";


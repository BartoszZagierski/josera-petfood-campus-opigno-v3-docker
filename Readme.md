# Opigno version 3.0.0-rc and Drupal version 9.x
## before installation add patch to fix problem but is not critical:

        "patches": {
          "opigno_catalog": {
              "Fixes #3248814": "https://www.drupal.org/files/issues/2021-11-10/opigno_catalog-fix_bad_current_user_define_options_default.patch"
          }
        },

